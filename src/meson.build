api_version = '0.1'

libodata_sources = [
  'od.c',
  'od-csdl.c',
  'od-csdl-document.c',
  'od-csdl-include.c',
  'od-csdl-include-annotations.c',
  'od-csdl-reference.c',
  'od-version.c',
]

libodata_headers = [
  'od.h',
  'od-csdl.h',
  'od-csdl-document.h',
  'od-csdl-include.h',
  'od-csdl-include-annotations.h',
  'od-csdl-reference.h',
]

version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]

version_conf = configuration_data()
version_conf.set('VERSION', meson.project_version())
version_conf.set('MAJOR_VERSION', MAJOR_VERSION)
version_conf.set('MINOR_VERSION', MINOR_VERSION)
version_conf.set('MICRO_VERSION', MICRO_VERSION)

configure_file(
  input: 'od-version.h.in',
  output: 'od-version.h',
  configuration: version_conf,
  install: true,
  install_dir: join_paths(get_option('includedir'), 'libodata-' + api_version)
)

libodata_deps = [
  dependency('gio-2.0', version: '>= 2.50'),
]

libodata_lib = shared_library('odata-' + api_version,
  libodata_sources,
  c_args: '-Wno-cast-function-type',
  dependencies: libodata_deps,
  install: true,
)

install_headers(libodata_headers, subdir: 'libodata-' + api_version)

pkg = import('pkgconfig')

pkg.generate(
  description: 'A shared library for ...',
    libraries: libodata_lib,
         name: 'libodata',
     filebase: 'libodata-' + api_version,
      version: meson.project_version(),
      subdirs: 'libodata',
     requires: 'glib-2.0',
  install_dir: join_paths(get_option('libdir'), 'pkgconfig')
)
