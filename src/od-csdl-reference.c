/* od-csdl-reference.c
 *
 * Copyright 2021 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "od-csdl-reference.h"

/**
 * SECTION:od-csdl-reference
 * @short_description:
 */

struct _OdCsdlReference
{
  GObject parent_instance;

  gchar *uri;
  GList *includes;
  GList *annotation_includes;
};

G_DEFINE_TYPE (OdCsdlReference,
               od_csdl_reference,
               G_TYPE_OBJECT);

enum
{
  PROP_0,
  PROP_URI,
  PROP_INC,
  PROP_ANNOT_INC,
  PROP_COUNT,
};

static GParamSpec *properties[PROP_COUNT];

static void
od_csdl_reference_init (OdCsdlReference *self)
{
  self->uri = NULL;
  self->includes = NULL;
  self->annotation_includes = NULL;
}

static void
od_csdl_reference_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  OdCsdlReference *self;

  g_assert (OD_IS_CSDL_REFERENCE (object));

  self = OD_CSDL_REFERENCE (object);

  switch (property_id)
    {
    case PROP_URI:
      g_assert (self->uri == NULL);

      self->uri = g_value_dup_string (value);
      break;

    case PROP_INC:
      g_assert (self->includes == NULL);

      self->includes = g_list_copy_deep (g_value_get_pointer (value),
                                         (GCopyFunc) g_object_ref,
                                         NULL);
      break;

    case PROP_ANNOT_INC:
      g_assert (self->annotation_includes);

      self->annotation_includes = g_list_copy_deep (g_value_get_pointer (value),
                                                    (GCopyFunc) g_object_ref,
                                                    NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
od_csdl_reference_get_property (GObject    *object,
                                guint       property_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  OdCsdlReference *self;

  g_assert (OD_IS_CSDL_REFERENCE (object));

  self = OD_CSDL_REFERENCE (object);

  switch (property_id)
    {
    case PROP_URI:
      g_value_set_string (value, self->uri);
      break;

    case PROP_INC:
      g_value_set_pointer (value, self->includes);
      break;

    case PROP_ANNOT_INC:
      g_value_set_pointer (value, self->annotation_includes);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
od_csdl_reference_class_init (OdCsdlReferenceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  properties[PROP_URI] =
    g_param_spec_string ("uri",
                         "Uri",
                         "The URI of the remote CSDL document",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_INC] =
    g_param_spec_pointer ("includes",
                          "Includes",
                          "GList* of all included schemas (not copied)",
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_ANNOT_INC] =
    g_param_spec_pointer ("annotation-includes",
                          "Annotation Includes",
                          "GList* of all included annotations (not copied)",
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties(object_class, PROP_COUNT, properties);

  object_class->get_property = od_csdl_reference_get_property;
  object_class->set_property = od_csdl_reference_set_property;
}

_OD_EXTERN
OdCsdlReference *
od_csdl_reference_new (const gchar *uri,
                       GList       *includes,
                       GList       *annotaion_includes)
{
  return g_object_new (OD_TYPE_CSDL_REFERENCE,
                       "uri", uri,
                       "includes", includes,
                       "annotation-includes", annotaion_includes,
                       NULL);
}

_OD_EXTERN
const gchar *
od_csdl_reference_get_uri (OdCsdlReference *self)
{
  g_assert (OD_IS_CSDL_REFERENCE (self));

  return self->uri;
}

_OD_EXTERN
GList *
od_csdl_reference_get_includes (OdCsdlReference *self)
{
  g_assert (OD_IS_CSDL_REFERENCE (self));

  return self->includes;
}

_OD_EXTERN
GList *
od_csdl_reference_get_annotaion_includes (OdCsdlReference *self)
{
  g_assert (OD_IS_CSDL_REFERENCE (self));

  return self->annotation_includes;
}
