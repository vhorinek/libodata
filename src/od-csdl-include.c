/* od-csdl-reference.h
 *
 * Copyright 2021 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "od-csdl-include.h"

struct _OdCsdlInclude
{
  GObject parent_instance;

  gchar *namespace;
  gchar *alias;
};

G_DEFINE_TYPE (OdCsdlInclude,
               od_csdl_include,
               G_TYPE_OBJECT);

enum
{
  PROP_0,
  PROP_NS,
  PROP_ALIAS,
  PROP_COUNT,
};

static GParamSpec *properties[PROP_COUNT] = { NULL, };

static void
od_csdl_include_init (OdCsdlInclude *self)
{
  self->namespace = NULL;
  self->alias = NULL;
}

static void
od_csdl_include_set_property (GObject      *object,
                              guint         property_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  OdCsdlInclude *self;

  g_assert (OD_IS_CSDL_INCLUDE (object));

  self = OD_CSDL_INCLUDE (object);

  switch (property_id)
    {
      case PROP_NS:
        self->namespace = g_value_dup_string (value);
        break;

      case PROP_ALIAS:
        self->alias = g_value_dup_string (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
od_csdl_include_get_property (GObject    *object,
                              guint       property_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  OdCsdlInclude *self;

  g_assert (OD_IS_CSDL_INCLUDE (object));

  self = OD_CSDL_INCLUDE (object);

  switch (property_id)
    {
      case PROP_NS:
        g_value_set_string (value, self->namespace);
        break;

      case PROP_ALIAS:
        g_value_set_string (value, self->alias);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
od_csdl_include_class_init (OdCsdlIncludeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  properties[PROP_NS] =
      g_param_spec_string ("namespace",
                           "Namespace",
                           "The imported namespace",
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_ALIAS] =
      g_param_spec_string ("alias",
                           "Alias",
                           "Alias for the imported namespace",
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_COUNT, properties);

  object_class->set_property = od_csdl_include_set_property;
  object_class->get_property = od_csdl_include_get_property;
}

_OD_EXTERN
OdCsdlInclude *
od_csdl_include_new (const gchar *nspace,
                     const gchar *alias)
{
  return g_object_new (OD_TYPE_CSDL_INCLUDE,
                       "namespace", nspace,
                       "alias", alias,
                       NULL);
}

_OD_EXTERN
const gchar *
od_csdl_include_get_namespace (OdCsdlInclude *self)
{
  g_assert (OD_IS_CSDL_INCLUDE (self));

  return self->namespace;
}

_OD_EXTERN
const gchar *
od_csdl_include_get_alias (OdCsdlInclude *self)
{
  g_assert (OD_IS_CSDL_INCLUDE (self));

  return self->alias;
}
