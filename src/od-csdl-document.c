/* od-csdl-document.c
 *
 * Copyright 2020-2021 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "od-csdl-document.h"

/**
 * SECTION:od-csdl-document
 * @short_description: Root component of any CSDL service description
 *
 * Represents the single entity model exposed by a service. It provides
 * a container for schemas defined by the service itself along with
 * ability to resolve references to schemas exposed by remote services.
 */

/**
 * OdCsdlDocument:
 */
struct _OdCsdlDocument
{
  GObject parent_instance;

  guint version_maj;
  guint version_min;
  GList *references;
};

G_DEFINE_TYPE (OdCsdlDocument,
               od_csdl_document,
               G_TYPE_OBJECT);

enum
{
  PROP_0,
  PROP_MAJOR,
  PROP_MINOR,
  PROP_REFERENCES,
  PROP_COUNT,
};

static GParamSpec *properties[PROP_COUNT] = { NULL, };

static void
od_csdl_document_init (OdCsdlDocument *self)
{
  self->version_maj = 4;
  self->version_min = 0;
  self->references = NULL;
}

static void
od_csdl_document_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  OdCsdlDocument *self;

  g_assert (OD_IS_CSDL_DOCUMENT (object));

  self = OD_CSDL_DOCUMENT (object);

  switch (property_id)
    {
    case PROP_MAJOR:
      self->version_maj = g_value_get_uint (value);
      break;

    case PROP_MINOR:
      self->version_min = g_value_get_uint (value);
      break;

    case PROP_REFERENCES:
      g_assert (self->references == NULL);

      self->references = g_list_copy_deep (g_value_get_pointer (value),
                                           (GCopyFunc) g_object_ref,
                                           NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
od_csdl_document_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  OdCsdlDocument *self;

  g_assert (OD_IS_CSDL_DOCUMENT (object));

  self = OD_CSDL_DOCUMENT (object);

  switch (property_id)
    {
    case PROP_MAJOR:
      g_value_set_uint (value, self->version_maj);
      break;

    case PROP_MINOR:
      g_value_set_uint (value, self->version_min);
      break;

    case PROP_REFERENCES:
      g_value_set_pointer (value, self->references);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
od_csdl_document_class_init (OdCsdlDocumentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  properties[PROP_MAJOR] =
    g_param_spec_uint ("version-major",
                       "Version Major",
                       "Major part of the document's version",
                       0,
                       G_MAXUINT,
                       4,
                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_MINOR] =
    g_param_spec_uint ("version-minor",
                       "Version Minor",
                       "Minor part of the document's version",
                       0,
                       G_MAXUINT,
                       0,
                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_REFERENCES] =
    g_param_spec_pointer ("references",
                          "References",
                          "GList*[OdCsdlReference] of references to other CSDL documents (no copy)",
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_COUNT, properties);

  object_class->set_property = od_csdl_document_set_property;
  object_class->get_property = od_csdl_document_get_property;
}

/**
 * od_csdl_document_new:
 */
_OD_EXTERN
OdCsdlDocument *od_csdl_document_new (void)
{
  return g_object_new (OD_TYPE_CSDL_DOCUMENT, NULL);
}

_OD_EXTERN
void            od_csdl_document_get_version      (OdCsdlDocument *self,
                                                   guint          *major,
                                                   guint          *minor)
{
  g_assert (OD_IS_CSDL_DOCUMENT (self));

  if (major != NULL)
    {
      *major = self->version_maj;
    }
  if (minor != NULL)
    {
      *minor = self->version_min;
    }
}

_OD_EXTERN
GList          *od_csdl_document_get_schemas      (OdCsdlDocument *self)
{
  g_assert (OD_IS_CSDL_DOCUMENT (self));

  g_critical ("Not implemented");

  return NULL;
}

_OD_EXTERN
GList          *od_csdl_document_get_references   (OdCsdlDocument *self)
{
  g_assert (OD_IS_CSDL_DOCUMENT (self));

  return self->references;
}
