/* od-csdl-include.h
 *
 * Copyright 2021 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#pragma once

#include <glib-object.h>

#include "od-version.h"

G_BEGIN_DECLS

#define OD_TYPE_CSDL_INCLUDE (od_csdl_include_get_type ())
G_DECLARE_FINAL_TYPE (OdCsdlInclude,
                      od_csdl_include,
                      OD, CSDL_INCLUDE,
                      GObject);

_OD_EXTERN
OdCsdlInclude *od_csdl_include_new           (const gchar *nspace,
                                                const gchar *alias);

_OD_EXTERN
const gchar   *od_csdl_include_get_namespace (OdCsdlInclude *self);

_OD_EXTERN
const gchar   *od_csdl_include_get_alias     (OdCsdlInclude *self);

G_END_DECLS
