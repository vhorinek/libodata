/* od-csdl-include-annotations.c
 *
 * Copyright 2021 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "od-csdl-include-annotations.h"

struct _OdCsdlIncludeAnnotations
{
  GObject parent_instance;

  gchar *term_namespace;
  gchar *qualifier;
  gchar *target_namespace;
};

G_DEFINE_TYPE (OdCsdlIncludeAnnotations,
               od_csdl_include_annotations,
               G_TYPE_OBJECT);

enum
{
  PROP_0,
  PROP_TERM,
  PROP_QUALIFIER,
  PROP_TARGET,
  PROP_COUNT,
};

static GParamSpec *properties[PROP_COUNT] = { NULL, };

static void
od_csdl_include_annotations_init (OdCsdlIncludeAnnotations *self)
{
  self->term_namespace = NULL;
  self->qualifier = NULL;
  self->target_namespace = NULL;
}

static void
od_csdl_include_annotations_set_property (GObject      *object,
                                          guint         property_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
  OdCsdlIncludeAnnotations *self;

  g_assert (OD_IS_CSDL_INCLUDE_ANNOTATIONS (object));

  self = OD_CSDL_INCLUDE_ANNOTATIONS (object);

  switch (property_id)
    {
    case PROP_TERM:
      g_assert (self->term_namespace == NULL);

      self->term_namespace = g_value_dup_string (value);
      break;

    case PROP_QUALIFIER:
      g_assert (self->qualifier == NULL);

      self->qualifier = g_value_dup_string (value);
      break;

    case PROP_TARGET:
      g_assert (self->target_namespace == NULL);

      self->target_namespace = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
od_csdl_include_annotations_get_property (GObject    *object,
                                          guint       property_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
  OdCsdlIncludeAnnotations *self;

  g_assert (OD_IS_CSDL_INCLUDE_ANNOTATIONS (object));

  self = OD_CSDL_INCLUDE_ANNOTATIONS (object);

  switch (property_id)
    {
    case PROP_TERM:
      g_value_set_string(value, self->term_namespace);
      break;

    case PROP_QUALIFIER:
      g_value_set_string(value, self->qualifier);
      break;

    case PROP_TARGET:
      g_value_set_string(value, self->target_namespace);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
od_csdl_include_annotations_class_init (OdCsdlIncludeAnnotationsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  properties[PROP_TERM] =
    g_param_spec_string ("term-namespace",
                         "Term Namespace",
                         "Namespace of imported annotation terms",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_QUALIFIER] =
    g_param_spec_string ("qualifier",
                         "Qualifier",
                         "Specifies subset of consumers for which the annotations will be included",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_TARGET] =
    g_param_spec_string ("target-namespace",
                         "Target Namespace",
                         "Only annotations applied to an item from this namespace will be included",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_COUNT, properties);

  object_class->set_property = od_csdl_include_annotations_set_property;
  object_class->get_property = od_csdl_include_annotations_get_property;
}

_OD_EXTERN
OdCsdlIncludeAnnotations *
od_csdl_include_annotations_new (const gchar *term_namespace,
                                 const gchar *qualifier,
                                 const gchar *target_namespace)
{
  return g_object_new (OD_TYPE_CSDL_INCLUDE_ANNOTATIONS,
                       "term-namespace", term_namespace,
                       "qualifier", qualifier,
                       "target-namespace", target_namespace);
}

_OD_EXTERN
const gchar *
od_csdl_include_annotations_get_term_namespace (OdCsdlIncludeAnnotations *self)
{
  g_assert (OD_IS_CSDL_INCLUDE_ANNOTATIONS (self));

  return self->term_namespace;
}

_OD_EXTERN
const gchar *
od_csdl_include_annotations_get_qualifier (OdCsdlIncludeAnnotations *self)
{
  g_assert (OD_IS_CSDL_INCLUDE_ANNOTATIONS (self));

  return self->qualifier;
}

_OD_EXTERN
const gchar *
od_csdl_include_annotations_get_target_namespace (OdCsdlIncludeAnnotations *self)
{
  g_assert (OD_IS_CSDL_INCLUDE_ANNOTATIONS (self));

  return self->target_namespace;
}
