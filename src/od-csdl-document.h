/* od-csdl-document.h
 *
 * Copyright 2020-2021 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#pragma once

#include <glib-object.h>

#include "od-version.h"

G_BEGIN_DECLS

#define OD_TYPE_CSDL_DOCUMENT (od_csdl_document_get_type ())
G_DECLARE_FINAL_TYPE (OdCsdlDocument,
                      od_csdl_document,
                      OD, CSDL_DOCUMENT,
                      GObject);

_OD_EXTERN
OdCsdlDocument *od_csdl_document_new              (void);

_OD_EXTERN
void            od_csdl_document_get_version      (OdCsdlDocument *self,
                                                   guint          *major,
                                                   guint          *minor);

_OD_EXTERN
GList          *od_csdl_document_get_schemas      (OdCsdlDocument *self);

_OD_EXTERN
GList          *od_csdl_document_get_references   (OdCsdlDocument *self);

G_END_DECLS
