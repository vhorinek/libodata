/* od-version.c
 *
 * Copyright 2020 Vilém Hořínek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "od-version.h"

_OD_EXTERN
guint    od_get_major_version (void)
{
  return OD_MAJOR_VERSION;
}

_OD_EXTERN
guint    od_get_minor_version (void)
{
  return OD_MINOR_VERSION;
}

_OD_EXTERN
guint    od_get_micro_version (void)
{
  return OD_MICRO_VERSION;
}

_OD_EXTERN
gboolean od_get_check_version (guint major,
                               guint minor,
                               guint micro)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtype-limits"
  return OD_CHECK_VERSION (major, minor, micro);
#pragma GCC diagnostic pop
}
